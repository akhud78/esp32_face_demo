# esp32_face_demo
- Test face recognition and detection


## Requirements
- Ubuntu 20.04 LTS
- [ESP-IDF v5.0.2](https://docs.espressif.com/projects/esp-idf/en/v5.0.2/esp32s3/index.html)

## Building
- Get ESP-IDF
```
$ mkdir -p ~/esp
$ cd ~/esp
$ git clone -b v5.0.2 --recursive https://github.com/espressif/esp-idf.git esp-idf-v5.0.2
$ cd esp-idf-v5.0.2/
$ ./install.sh
```
- Checking
```
$ . $HOME/esp/esp-idf-v5.0.2/export.sh
$ idf.py --version
ESP-IDF v5.0.2-dirty
```

- Clone and setup [project](https://gitlab.com/akhud78/esp32_face_demo)

```
$ cd ~/esp
$ git clone https://gitlab.com/akhud78/esp32_face_demo
$ cd ~/esp/esp32_face_demo
$ git submodule update --init --recursive
```

- Configuration
```
$ cd test
$ idf.py set-target esp32s3
$ idf.py menuconfig
```
- [Storage](components/storage/README.md)
- [Image](components/image/README.md)
- [Face](components/face/README.md)


## Usage
- You must store [face_test](components/face/face_test) and [face_setup](components/face/face_setup) directories into SD card!

```
$ cd ~/esp/esp32_face_demo/test/
$ rm build/CMakeCache.txt
$ idf.py -p /dev/ttyACM0 flash monitor
```
### Interactive test menu
- Press ENTER to see the list of tests.
```
Here's the test menu, pick your combo:
(1)	"face detection" [face]
(2)	"face recognition" [face]

```
- [Test log output](test_output_log.txt)
